# 一、iRedMail - 开源免费的邮件系统解决方案
    iRedMail 以最佳的方式搭建基于 Linux/BSD 和开源软件的邮件服务器，始于 2007 年。支持 Red Hat Enterprise Linux、CentOS、Debian、Ubuntu、FreeBSD、OpenBSD 等操作系统。
    它实际是一系列开源软件的集合，提供了基于 web 访问的后台管理系统，使用 RoundCube 作为 web 邮件系统。同时还支持各种邮件客户端，支持iOS、安卓系统的邮件和日历同步。
    * 授权费用
        iredmail 提供了免费版和收费版，免费版就可以满足大部分环境的需求，如果需要收费版，可以访问 官方网站的价格页面 询问价格。
    * 应用特性
        -数据私密性
            数据均存储在您的服务器上，您可以控制邮件安全性，检查各种日志。没有中间运营商，没有人能看到您的数据。
        -开源／信任
            iRedMail 使用的所有组件都是开源软件，并且直接从您信任的 Linux/BSD 发行版官方获取软件更新。
        -默认即安全
            终端用户必须使用加密的连接（如基于 TLS 的 POP3/IMAP/SMTP 服务，HTTPS），邮件服务器之间的传输也是加密的，用户密码以高强度的 SSHA512 或 BCRYPT （仅 BSD 系统可用）算法加密存储。
        -Webmail
            使用直观的 webmail 程序管理您的邮件、邮箱目录、过滤规则、假期自动回复等
        -日历、联系人、ActiveSync
            使用美观易用的 webmail 界面管理日历（CalDAV）、地址簿（CardDAV）、任务。完美支持主流移动设备（iOS、Android、BlackBerry 10、Windows Phone）。
            日历、地址簿、任务、ActiveSync 皆由 SOGo Groupware 程序提供。
        -无限数量的邮件帐号
            忘掉那些按用户数收费的产品吧，iRedMail 允许您创建任意数量的邮件域、邮件帐号，而且－－免费。
        -支持主流的 Linux/BSD 发行版
            iRedMail 支持 Red Hat Enterprise Linux、CentOS、Debian、Ubuntu、FreeBSD、OpenBSD。不论使用哪个发行版，都只需要几分钟即可部署一台配置参数一模一样的邮件服务器。
        -存储后端
            支持将邮件帐号存储在 OpenLDAP、MySQL、MariaDB、PostgreSQL。
        -反垃圾和防病毒
            集成 SpamAssassin 和 ClamAV，支持 SPF、DKIM、灰名单、黑白名单、第三方 DNSBL 服务。支持将检测到的垃圾和病毒邮件隔离到 SQL 数据库以便后期管理。
        -Web 管理平台
            iRedMail 提供直观易用的 web 程序管理您的帐号和邮件服务器设置。另有独立收费产品 iRedAdmin-Pro 提供更多高级功能，详情请访问 iRedAdmin-Pro 功能列表。
        -可重现的部署
            每次部署都和之前的一模一样，易于迁移和备份，快速、稳定。

# 二、iRedMail Docker Container

iRedMail allows to deploy an OPEN SOURCE, FULLY FLEDGED, FULL-FEATURED mail server in several minutes, for free. If several minutes is long time then this docker container can reduce help you and deploy your mail server in seconds.

Current version of container uses MySQL for accounts saving. In the future the LDAP can be used, so pull requests are welcome. Container contains all components (Postfix, Dovecot, Fail2ban, ClamAV, Roundcube, and SoGo) and MySQL server. The hostname of the mail server can be set using the normal docker methods (docker run -h <host> or setting 'hostname' in a docker compose file). In order to customize the container several environmental variables are allowed:

MYSQL_ROOT_PASSWORD - Root password for MySQL server installation

POSTMASTER_PASSWORD - Initial password for postmaster@DOMAIN. Password can be generated according to wiki. ({PLAIN}password)

TZ - Container timezone that is propagated to other components

SOGO_WORKERS - Number of SOGo workers which can affect SOGo interface performance.

Container is prepared to handle data as persistent using mounted folders for data. Folders prepared for initialization are:PATH/

/var/lib/mysql

/var/vmail

/var/lib/clamav

With all information prepared, let's test your new iRedMail server:

    docker run --privileged -p 80:80 -p 443:443 -p 25:25 -p 465:465 -p 110:110 -p 143:143 -p 587:587 -p 993:993 -p 995:995 \
           -h iredmail.expresscare.cn \
           -e "MYSQL_ROOT_PASSWORD=Gmccgmcc1" \
           -e "SOGO_WORKERS=2" \
           -e "TZ=Asia/Shanghai" \
           -e "POSTMASTER_PASSWORD={PLAIN}Gmccgmcc1" \
           -e "IREDAPD_PLUGINS=['reject_null_sender', 'reject_sender_login_mismatch', 'throttle', 'amavisd_wblist', 'sql_alias_access_policy']" \
           -v /home/ubuntu/iredmail/mysql:/var/lib/mysql \
           -v /home/ubuntu/iredmail/vmail:/var/vmail \
           -v /home/ubuntu/iredmail/clamav:/var/lib/clamav \
           -v /home/ubuntu/iredmail/config/letsencrypt:/etc/letsencrypt \
           --name=iredmail lf1707/iredmail
    
    # 关于lf1707/iredmail镜像的说明
        - 基于lemr/iredmail的github改编：https://github.com/lejmr/iredmail-docker
        - 修改main.cf,master.cf: 增加了465端口开启，取消reject_unknown_helo_hostname选项
        - 修改rc.local: 增加letsencrypt认证文件的判断和自动连接
           
# 三、安装步骤
    3.1 选择安装的目录，cd ~/iredmail
    3.2 git clone ssh://git@gitlab.expresscare.cn:10022/linfang/iredmail.git
    3.3 替换 ./config/letsencrypt/live/iredmail.expresscare.cn中的四个认证文件
        -注意，改认证文件由letsencrypt生成，跟随hostname，本例中即为iredmail.expresscare.cn
        -如果变更机器域名，需要同步重写
    3.4 运行 README中的docker run指令
    
# 四、iRedMail相关的管理后台登录地址：
    Roundcube webmail: http://iredmail.expresscare.cn/mail/
    iRedAdmin: http://iredmail.expresscare.cn/iredadmin/
    phpLDAPadmin：http://iredmail.expresscare.cn/phpldapadmin/
    phpMyAdmin http://iredmail.expresscare.cn/phpmyadmin/
    CalDAV and CardDAV server addresses: https://iredmail.expresscare.cn/SOGo/dav/<full email address>
    Awstats: http://iredmail.expresscare.cn/awstats/awstats.pl?config=web (或者 ?config=smtp)
    官网文档：https://docs.iredmail.org/index.html
    
# 五、客户端发送大附件。如果允许邮件客户端发送大附件，需要运行
    postconf -e message_size_limit='104857600'   #把附件增加到100m
# 六、webmail。如果允许webmail发送大附件，需要多修改2个地方 ，
    首先是编辑/etc/php5/apache2/php.ini 文件
        - upload_max_filesize = 100M;
        - post_max_size = 100M;
    然后是修改/usr/share/apache2/roundcube/.htaccess 文件
        - upload_max_filesize    100M
        - post_max_size    100M
# 七、关于sohu,解决能发不能收的问题，log显示450 4.7.1 Helo command rejected: Host not found
    - 需要在/etc/postfix/main.cf中去掉unknown_helo_hostname：
        smtpd_helo_restrictions = 
            permit_mynetworks 
            permit_sasl_authenticated 
            reject_non_fqdn_helo_hostname
            smtpd_helo_restrictions =
          #reject_unknown_helo_hostname
    
    - reload the postfix daemon

# 八、DKIM
    
# 九、iRedmail使用的组件
|Name|Comment|
|-|-|
|Postfix|Mail Transfer Agent (MTA)|
|Dovecot	|POP3, IMAP and Managesieve server|
|Apache，Nginx	|Web server|
|OpenLDAP, ldapd(8)	|LDAP server, used for storing mail accounts (optional)|
|MySQL, MariaDB, PostgreSQL	|SQL server used to store application data. Could be used to store mail accounts too.|
|mlmmj	|Mailing list manager. Shipped in iRedMail-0.9.8 and later releases.|
|Amavisd-new	|Interface between Postfix and SpamAssassin, ClamAV.|
|SpamAssassin	|Content-based spam scanner|
|ClamAV	|Virus scanner|
|Roundcube	|Webmail (PHP)|
|SOGo Groupware	|A groupware which provides calendar (CalDAV), contact (CardDAV), tasks and ActiveSync services|
|Fail2ban	|Scans log files and bans IPs that show the malicious signs|
|Awstats	|Apache and Postfix log analyzer|
|iRedAPD	|A postfix policy server developed by iRedMail team|

# 十、iRedMail使用的端口说明
# 端口465
    已被放弃，转向587，原镜像不再使用，为兼容性，本镜像设置打开：
    - 在/etc/postfix/master.cf中增加以下：
        465     inet  n       -       n       -       -       smtpd
            -o syslog_name=postfix/smtps
            -o smtpd_tls_wrappermode=yes
            -o smtpd_sasl_auth_enable=yes
            -o smtpd_client_restrictions=permit_sasl_authenticated,reject
            -o content_filter=smtp-amavis:[127.0.0.1]:10026
    - reload the postfix daemon
    
|Port|Service|Software|Comment|Allow Public Access|
|-|-|-|---|-|
|25|smtp|Postfix|Normal smtp service, used for server-to-server communication. WARNING: This port MUST be open, otherwise you cannot receive email sent from other servers.	|YES|
|587|submission|Postfix	a.k.a. SMTP over TLS. |Used by end users to send/submit email.|YES (open to your end users)|
|24|lmtp|Dovecot|Used to deliver email to local mailboxes via LMTP protocol.|No (listen on 127.0.0.1 by default)|
|110|pop3|Dovecot|Used by end users to retrieve emails via POP3 protocol,secure connection over STARTTLS is available by default.|YES (open to your end users)|
|995|pop3s|Dovecot|Used by end users to retrieve emails via POP3 protocol over SSL.Port 110 with STARTTLS is recommended.|YES (open to your end users)|
|143|imap|Dovecot|Used by end users to retrieve emails via IMAP protocol,secure connection over STARTTLS is available by default.|YES (open to your end users)|
|993|imaps|Dovecot|Used by end users to retrieve emails via IMAP protocol over SSL.Port 143 with STARTTLS is recommended.|YES (open to your end users)|
|4190|manage|sieve|Dovecot|Sieve service used by end users to manage mail filters.Note: in old iRedMail releases, it's port 2000 (deprecated and not even listed in /etc/services file).|NO (disabled by default and users are forced to manage mail filters with webmail)|
|80|http|Apache/Nginx|Web service|YES (open to your webmail users)|
|443|https|Apache/Nginx	Web service over over SSL, secure connection. |SOGo groupware provides Exchange ActiveSync (EAS) support through port 443.|YES (open to your webmail users)|
|3306|mysql|MySQL/MariaDB|MySQL/MariaDB database service|NO (listen on 127.0.0.1 by default)|
|5432|postgresql|PostgreSQL|PostgreSQL database service|NO (listen on 127.0.0.1 by default)|
|389|ldap|OpenLDAP (or OpenBSD ldapd)	|LDAP service, STARTTLS is available for secure connection.|NO (listen on 127.0.0.1 by default)|
|636|ldaps|OpenLDAP (or OpenBSD ldapd)	|LDAP service over SSL. Deprecated, port 389 with STARTTLS is recommended.|NO (listen on 127.0.0.1 by default)|
|10024||Amavisd-new|Used to scan inbound messages, includes spam/virus scanning, DKIM verification, applying spam policy.|NO (listen on 127.0.0.1 by default)|
|10025|smtp|Postfix|Used by Amavisd to inject scanned emails back to Postfix queue.|NO (listen on 127.0.0.1 by default)|
|10026||Amavisd-new|Used to scan outbound messages, includes spam/virus scanning, DKIM signing, applying spam policy.|NO (listen on 127.0.0.1 by default)|
|10027||Amavisd-new|Used by mlmmj mailing list manager, it bypasses spam/virus/header/banned checks by default, but have DKIM signing enabled.|NO (listen on 127.0.0.1 by default)|
|9998||Amavisd-new|Used to manage quarantined emails.|NO (listen on 127.0.0.1 by default)|
|7777||iRedAPD|Postfix policy service for greylisting, whitelisting, blacklists, throttling, etc|NO (listen on 127.0.0.1 by default)|
|7790|http|mlmmjadmin|RESTful API server used to manage mlmmj mailing lists. New in iRedMail-0.9.8.|NO (listen on 127.0.0.1 by default)|
|20000||SOGo|SOGo groupware|NO (listen on 127.0.0.1 by default)|
|11211||Memcached|A distributed, high performance memory object caching system.|No (listen on 127.0.0.1 by default)|
|24242||Dovecot|Dovecot service status. New in iRedMail-0.9.8.|NO (listen on 127.0.0.1 by default)|
|19999||Netdata|Netdata monitor. New in iRedMail-0.9.8.|NO (listen on 127.0.0.1 by default)|
